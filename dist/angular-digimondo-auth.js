(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('digimondoAuth.config', [])
      .value('digimondoAuth.config', {
          debug: true
      });

  // Modules
  angular.module('digimondoAuth.directives', []);
  angular.module('digimondoAuth.filters', []);
  angular.module('digimondoAuth.services', []);
  angular.module('digimondoAuth',
      [
          'digimondoAuth.config',
          'digimondoAuth.directives',
          'digimondoAuth.filters',
          'digimondoAuth.services',
          'ngCookies',
          'ngSanitize'
      ]);
  angular.module('digimondoAuth.services')
    .config(function ($httpProvider) {
      $httpProvider.interceptors.push('authHttpInterceptor');
    });
})(angular);

angular.module('digimondoAuth.services')
  .service('$account', function($q, $http, $digimondoAuth, $rootScope, $injector, $location) {
    var account = null;
    var token = null;
    var $state = null;
    var transitioning = false;

    var TOK = '[digimondoAuth]';


    if ($injector.has('$state')) {
      $state = $injector.get('$state');

      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.data && toState.data.requireAuth && !transitioning) {
          event.preventDefault();
          console.log(TOK, 'Waiting for user to auth');
          $account.isLoggedIn().then(function(loggedIn) {
            if (loggedIn) {
              console.log(TOK, 'Navigating to state', toState);
              transitioning = true;
              $state.go(toState, toParams);
              transitioning = false;
            } else {
              console.log(TOK, 'Not navigating to route because user is not authed. Going back to', fromState);
              if (fromState.abstract) {
                $location.path('/');
              } else {
                $state.go(fromState, fromParams);
              }
            }
          });
        }
      });
    } else {
      console.log(TOK, 'not using ui-route to protect routes from unauthorized access.');
    }


    var $account = {
      getToken: function() {
        if (!token) {
          return $q.reject('No token!');
        }
        return token;
      },

      getProfile: function() {
        if (account === null && token === null) {
          return $q.reject('Not logged in');
        }
        if (account === null && token !== null) {
          account = $http.get($digimondoAuth.getBackend() + '/api/profile', {token: token, withCredentials: true})
            .then(function (res) {
              if (res.status === 200) {
                return res.data;
              } else {
                return $q.reject(res.data);
              }
            });
        }
        return account;
      },

      isLoggedIn: function() {
        if (token === null) {
          // try to log in from previous session cookie
          var deferredToken = $q.defer();
          token = deferredToken.promise;
          return $http.get($digimondoAuth.getBackend() + '/api/auth/session', {withCredentials: true})
            .then(function(res) {
              if (res.status === 200) {
                deferredToken.resolve(res.data.token);
                $rootScope.$emit('loggedIn');
                return true;
              } else {
                deferredToken.reject(res.data);
                return false;
              }
            })
            .catch(function(res) {
              deferredToken.reject();
              return false;
            });
        } else {
          return token
            .then(function(token) {
              return token !== null && !token.err;
            })
            .catch(function(err) {
              return false;
            });
        }
      },

      login: function(email, pass, scopes) {
        if (scopes === null || scopes.length === 0) {
          scopes = ['auth'];
        }
        var deferredToken = $q.defer();
        token = deferredToken.promise;
        return $http.post($digimondoAuth.getBackend() + '/api/auth/session', {
              email: email,
              pass: pass,
              scopes: scopes
            }, {withCredentials: true})
            .then(function (res) {
              if (res.status === 200 && res.data.token) {
                $rootScope.$emit('loggedIn');
                deferredToken.resolve(res.data.token);
                return token;
              } else {
                deferredToken.reject(res.data);
                return $q.reject(res.data);
              }
            })
            .catch(function(res) {
              return $q.reject(res.data.err)
            });
      },

      logout: function() {
        if (token) {
          return $http.delete($digimondoAuth.getBackend() + '/api/auth/session', {withCredentials: true})
            .then(function(res) {
              if (res.data.logout) {
                token = null;
                account = null;
                $rootScope.$emit('loggedOut');
                return res.data;
              } else {
                return $q.reject(res.data.err);
              }
            })
        }
      }
    };

    return $account;
  });

angular.module('digimondoAuth.services')
  .factory('authHttpInterceptor', function($q, $digimondoAuth, $injector) {
    function parseURI(uri) {
      var parser = document.createElement('a');
      parser.href = uri;
      return parser;
    }

    var $account = null;

    return {
      request: function(config) {
        if ($account === null) {
          $account = $injector.get('$account');
        }

        var url = config.url;
        if ($digimondoAuth.getApi().exec(url)) {
          if ($account.getToken() !== null) {
            return $account.getToken()
              .then(function(token) {
                config.headers['Authorization'] = 'Bearer ' + token;
                return config;
              })
          } else {
            return config;
          }
        } else {
          return config;
        }
      }
    }
  });

angular.module('digimondoAuth.services')
  .provider('$digimondoAuth', function $digimondoAuthProvider() {
    var backend = null;
    var api = null;

    this.config = function(options) {
      api = options.api || null;
      backend = options.backend || null;
    };

    this.$get = function() {
      return {
        getApi: function () {
          return api;
        },

        getBackend: function () {
          return backend;
        }
      };
    };
  });
