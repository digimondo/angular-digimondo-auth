angular.module('digimondoAuth.services')
  .provider('$digimondoAuth', function $digimondoAuthProvider() {
    var backend = null;
    var api = null;

    this.config = function(options) {
      api = options.api || null;
      backend = options.backend || null;
    };

    this.$get = function() {
      return {
        getApi: function () {
          return api;
        },

        getBackend: function () {
          return backend;
        }
      };
    };
  });
