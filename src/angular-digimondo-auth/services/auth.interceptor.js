angular.module('digimondoAuth.services')
  .factory('authHttpInterceptor', function($q, $digimondoAuth, $injector) {
    function parseURI(uri) {
      var parser = document.createElement('a');
      parser.href = uri;
      return parser;
    }

    var $account = null;

    return {
      request: function(config) {
        if ($account === null) {
          $account = $injector.get('$account');
        }

        var url = config.url;
        if ($digimondoAuth.getApi().exec(url)) {
          if ($account.getToken() !== null) {
            return $account.getToken()
              .then(function(token) {
                config.headers['Authorization'] = 'Bearer ' + token;
                return config;
              })
          } else {
            return config;
          }
        } else {
          return config;
        }
      }
    }
  });
