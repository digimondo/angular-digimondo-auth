(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('digimondoAuth.config', [])
      .value('digimondoAuth.config', {
          debug: true
      });

  // Modules
  angular.module('digimondoAuth.directives', []);
  angular.module('digimondoAuth.filters', []);
  angular.module('digimondoAuth.services', []);
  angular.module('digimondoAuth',
      [
          'digimondoAuth.config',
          'digimondoAuth.directives',
          'digimondoAuth.filters',
          'digimondoAuth.services',
          'ngCookies',
          'ngSanitize'
      ]);
  angular.module('digimondoAuth.services')
    .config(function ($httpProvider) {
      $httpProvider.interceptors.push('authHttpInterceptor');
    });
})(angular);
